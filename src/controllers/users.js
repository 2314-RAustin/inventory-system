import models from '../models'

export default class users {
    static async get(req, res) {
        const items = await models.users.findAll()

        return res.json({
            success: false,
            message: items,
        });
    }
}