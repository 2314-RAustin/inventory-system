import models from '../models'
const Op = models.Sequelize.Op

const excecutePromises = async (functions = []) => {
    if(functions.length > 0){
        await Promise.all(functions)
        .then(({dataValues}) => {
        })
        .catch(err => {
            console.log('err ', err);
        })
    }
}

const buildWhere = (querys) => {
    let where = {};

    if(Array.isArray(querys)){
        for (const query of querys) {
            let {type, fields} = query;
            if(type === 'equals'){
                for (let field of fields) {
                    for (const key in field) {
                        if (field.hasOwnProperty.call(field, key)) {
                            const element = field[key];
                            where[key] = element;
                        }
                    }
                }
            }
        }
    } else {
        if ('id_company' in querys) {
            where['id_company'] = {[Op.like]:'%'+querys.id_company+'%'}
        }
    
        if ('empresa' in querys) {
            where['empresa'] = {[Op.like]:'%'+querys.empresa+'%'}
        }
    
        if ('telefono' in querys) {
            where['telefono'] = {[Op.like]:'%'+querys.telefono+'%'}
        }
    
        if ('contacto' in querys) {
            where['contacto'] = {[Op.like]:'%'+querys.contacto+'%'}
        }
    
        if ('correo_electronico' in querys) {
            where['correo_electronico'] = {[Op.like]:'%'+querys.correo_electronico+'%'}
        }
    
        if ('giro' in querys) {
            where['giro'] = {[Op.like]:'%'+querys.giro+'%'}
        }
    
        if ('convenio' in querys) {
            where['convenio'] = {[Op.like]:'%'+querys.convenio+'%'}
        }
    
        if ('comentarios' in querys) {
            where['comentarios'] = {[Op.like]:'%'+querys.comentarios+'%'}
        }
    
        where['active'] = true;
    }

    return where;
}

export default class company {
    static async get(req, res) {
        if(req.body.query){
            const {query} = req.body;
            const companys = await models.companys.findAll({where: buildWhere(query)})
            return res.json({
                success: true,
                message: "Succes",
                response: companys
            });
        } else {
            const companys = await models.companys.findAll();
            return res.json({
                success: true,
                message: "Succes",
                response: companys
            });
        }
    }

    static async create(req, res) {
        if(req.body.data){
            if(req.body.data.length > 1){
                let functions = [];
                for (const element of req.body.data) {
                    functions.push(models.companys.create(element));
                }
                await excecutePromises(functions);
                return res.json({
                    success: true,
                    message: "Succes",
                });
            } 
        } else if(req.body){
            models['companys'].create(req.body)
            .then(resp => {
                return res.json({
                    success: true,
                    message: "Succes",
                    response: resp
                });
            })
            .catch(err => {
                // console.error('err ', err)
                return res.json({
                    success: false,
                    message: "Error",
                    response: err.message
                });
            })
        }
    }

    static async update(req, res) {
        if(req.body.data){
            if(req.body.data.length > 1){
                let functions = [];
                for (const element of req.body.data) {
                    let {id_company} = element;
                    delete element['id_company'];
                    functions.push(models.companys.update(element, {
                        where:{
                            id_company
                        }
                    }));
                }
                await excecutePromises(functions);
                return res.json({
                    success: true,
                    message: "Succes",
                });
            } 
        } else if(req.body){
            const {id_company} = req.body;
            delete req.body['id_company']
            models['companys'].update(req.body, {
                where:{
                    id_company
                }
            })
            .then(resp => {
                return res.json({
                    success: true,
                    message: "Succes",
                    response: resp
                });
            })
            .catch(err => {
                return res.json({
                    success: false,
                    message: "Error",
                    response: err.message
                });
            })
        }
    }

    static async delete(req, res) {
        if(req.body){
            const {id_company} = req.body;
            models['companys'].update({
                active: false
            }, {
                where:{
                    id_company
                }
            })
            .then(resp => {
                return res.json({
                    success: true,
                    message: "Succes",
                    response: resp
                });
            })
            .catch(err => {
                return res.json({
                    success: false,
                    message: "Error",
                    response: err.message
                });
            })
        }
    }
}