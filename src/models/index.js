import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize'
import config from '../../config.json'
const mode = process.env.NODE_ENV || config.mode;
// const info = config[mode]['database']['inventory'];
const info = config.development.database.inventory;
const { host, database, port } = info
console.log(`🗄  Database host:${host} , database:${database} , port:${port}`)

const sequelize = new Sequelize(database, info.username, info.password, {
    host: info.host,
    port: info.port,
    dialect: info.dialect,
    pool: {
        max: 30,
        min: 0,
        idle: 650000,
        acquire: 1000000,
    },
    define: {
        timestamps: false,
    },
    dialectOptions: {
        multipleStatements: true,
    },
    timezone: '-08:00',
    // logging: true,
});

(async () => {
    try {
        await sequelize.authenticate()
        console.log('Connection has been established successfully.')
    } catch (error) {
        console.error('Unable to connect to the database:', error)
    }
})()

let db = {}

fs.readdirSync(__dirname)
    .filter((file) => {
        return (
            file.indexOf('.') !== 0 &&
            file !== 'index.js' &&
            file !== 'mongodb' &&
            file !== 'gglobalization'
        )
    })
    .forEach((file) => {
        const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes)
        db[model.name] = model
    })

Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db)
    }
})

db.sequelize = sequelize
db.Sequelize = Sequelize
db.sequelize.dialect.supports.schemas = true

module.exports = db