module.exports = function (sequelize, DataTypes) {
    let Model = sequelize.define("companys",
        {
            id_company: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            empresa: {
                type: DataTypes.STRING(200),
                allowNull: false
            },
            telefono: {
                type: DataTypes.CHAR(100),
                allowNull: true
            },
            contacto: {
                type: DataTypes.STRING(200),
                allowNull: false
            },
            correo_electronico: {
                type: DataTypes.STRING(150),
                allowNull: false
            },
            giro: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            convenio: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            comentarios: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            active: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: true,
            },
        },
        {
            tableName: "companys"
        });

    return Model;
};