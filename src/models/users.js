module.exports = function (sequelize, DataTypes) {
    let Model = sequelize.define("users",
        {
            id_user: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            user: {
                type: DataTypes.STRING(45),
                allowNull: false
            },
            password: {
                type: DataTypes.CHAR(45),
                allowNull: true
            }
        },
        {
            tableName: "users"
        });

    return Model;
};