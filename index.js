import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import busboyBodyParser from 'busboy-body-parser';
import cors from 'cors'
import config from './config'

const mode = config.mode || process.env.NODE_ENV || 'development';
const port = config[mode].port;
const app = express()

app.set('port', port)
app.use(compression())
app.use(bodyParser.json({ limit: '50mb', type: '*/json' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }))
app.use(bodyParser.text({ type: 'application/pkcs7-mime' }))
app.use(busboyBodyParser())


app.use(
  cors({
      origin: (origin, callback) => {
          callback(null, true)
      },
      methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
      exposedHeaders: ['Content-Disposition'],
      credentials: true,
  })  
)

app.use('/', require('./router.js'))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});