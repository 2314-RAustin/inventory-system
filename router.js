import controllers from './src/controllers';
import express from 'express';
const router = express.Router()

router.get('/', (req, res) => {res.send('Hola desde Api-Wms')})
router.route('/user').get(controllers.users.get);

//Company
router.route('/company').get(controllers.company.get);
router.route('/company').post(controllers.company.create);
router.route('/company').put(controllers.company.update);
router.route('/company').delete(controllers.company.delete);

module.exports = router;